/*
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                     Version 2, December 2004
 *
 *  Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 *  Everyone is permitted to copy and distribute verbatim or modified
 *  copies of this license document, and changing it is allowed as long
 *  as the name is changed.
 *
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *   0. You just DO WHAT THE FUCK YOU WANT TO.
 *
 */

package index.transformine.Updater;

import index.transformine.Main;

import java.util.ArrayList;
import java.util.List;

public class DatabaseUpdater {
    
    private List<DatabaseUpdate> updates = null;

    public DatabaseUpdater() {
        super();
        
        this.updates = new ArrayList<DatabaseUpdate>();
    }
    
    public void execute() {
        //this.updates.add(new DatabaseUpdate("ALTER TABLE `" + DatabaseManager.DBPrefix + PlayerStatistic.tableName + "` ADD `games` INT(11) NOT NULL;"));
        
        this.executeUpdates();
    }
    
    private void executeUpdates() {
        for(DatabaseUpdate update : this.updates) {
            try {
                Main.getInstance().getDatabaseManager().execute(update.getSql());
            } catch(Exception ex) {
                // nothing ;)
            }
        }
    }

}
