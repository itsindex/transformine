/*
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                     Version 2, December 2004
 *
 *  Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 *  Everyone is permitted to copy and distribute verbatim or modified
 *  copies of this license document, and changing it is allowed as long
 *  as the name is changed.
 *
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *   0. You just DO WHAT THE FUCK YOU WANT TO.
 *
 */

package index.transformine.Game;

/**
 * Created by ksnzk on 24.02.2016.
 */
public enum RoomMode {
    VANILA("vanila"), NORMAL("normal"), BUILDING("building");

    private final String text;

    /**
     * @param text
     */
    private RoomMode(final String text) {
        this.text = text;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }

    public static RoomMode getModeByName(String name) {
        for (RoomMode mode : RoomMode.values()) {
            if (name.contains(mode.toString()) && mode!=BUILDING) {
                return mode;
            }
        }
        return NORMAL;
    }
}
