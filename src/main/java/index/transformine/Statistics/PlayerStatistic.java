/*
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                     Version 2, December 2004
 *
 *  Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 *  Everyone is permitted to copy and distribute verbatim or modified
 *  copies of this license document, and changing it is allowed as long
 *  as the name is changed.
 *
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *   0. You just DO WHAT THE FUCK YOU WANT TO.
 *
 */

package index.transformine.Statistics;

import index.transformine.Database.DBGetField;
import index.transformine.Database.DBSetField;
import index.transformine.Main;
import index.transformine.UUIDFetcher;
import org.bukkit.OfflinePlayer;

import java.util.UUID;

public class PlayerStatistic extends StoringTable {

	public static final String tableName = "stats_players";

	private OfflinePlayer player = null;
	private UUID uuid = null;

	private int firsts = 0;
	private int score = 0;
	private int currentScore = 0;

	private boolean once = false;

	public PlayerStatistic() {
		super();
	}

	public PlayerStatistic(OfflinePlayer player) {
		super();

		this.player = player;
	}

	public OfflinePlayer getPlayer() {
		return this.player;
	}

	public int getCurrentScore() {
		return this.currentScore;
	}

	public void addCurrentScore(int score) {
		this.currentScore += score;
	}

	public void setCurrentScore(int score) {
		this.currentScore = score;
	}

	@DBGetField(name = "uuid", dbType = "VARCHAR(255)")
	public String getUUID() throws Exception {
		if(this.uuid == null) {
			try {
				if(this.player.isOnline()) {
					this.uuid = this.player.getPlayer().getUniqueId();
				} else {
					this.uuid = this.player.getUniqueId();
				}
			} catch(Exception ex) {
				this.uuid = UUIDFetcher.getUUIDOf(this.player.getName());
			}
		}

		return this.uuid.toString();
	}

	@DBGetField(name = "name", dbType = "VARCHAR(255)")
	public String getName() {
		return this.player.getName();
	}

	@DBGetField(name = "firsts", dbType = "INT(11)", defaultValue = "0")
	@StatField(name = "firsts", order = 60)
	public int getGames() {
		return this.firsts;
	}

	@DBGetField(name = "score", dbType = "INT(11)", defaultValue = "0")
	@StatField(name = "score", order = 70)
	public int getScore() {
		return score;
	}

	@DBSetField(name = "score")
	public void setScore(int score) {
		this.score = score;
	}

	@Override
	public String getKeyField() {
		return "uuid";
	}

	@Override
	public void load() {
		Main.getInstance().getPlayerStatisticManager().loadStatistic(this);
	}

	public void setOnce(boolean once) {
		this.once = once;
	}

	public boolean isOnce() {
		return this.once;
	}

	@Override
	public void store() {
		Main.getInstance().getPlayerStatisticManager().storeStatistic(this);
	}

	@Override
	public void setDefault() {
		this.firsts = 0;
		this.score = 0;
	}

	@Override
	public String getTableName() {
		return "stats_players";
	}

}
