/*
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                     Version 2, December 2004
 *
 *  Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 *  Everyone is permitted to copy and distribute verbatim or modified
 *  copies of this license document, and changing it is allowed as long
 *  as the name is changed.
 *
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *   0. You just DO WHAT THE FUCK YOU WANT TO.
 *
 */

package index.transformine;

import com.google.common.collect.ImmutableMap;
import com.intellectualcrafters.plot.api.PlotAPI;
import index.transformine.Commands.BaseCommand;
import index.transformine.Commands.RoomCommand;
import index.transformine.Commands.SaveMapCommand;
import index.transformine.Database.DatabaseManager;
import index.transformine.Game.RoomManager;
import index.transformine.Listener.PlayerListener;
import index.transformine.Localization.LocalizationConfig;
import index.transformine.Statistics.PlayerStatisticManager;
import index.transformine.Statistics.StorageType;
import index.transformine.Updater.ConfigUpdater;
import index.transformine.Updater.DatabaseUpdater;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.*;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Logger;

public class Main extends JavaPlugin {

    private Package craftbukkit = null;
    private Package minecraft = null;
    private String version = null;
    private LocalizationConfig localization = null;
    private PlayerStatisticManager playerStatisticManager = null;
    private DatabaseManager dbManager = null;
    private ArrayList<BaseCommand> commands = new ArrayList<>();

    private static Main instance = null;
    private static Logger logger = Bukkit.getLogger();
    public PlotAPI api;
    private RoomManager roomManager;

    @Override
    public void onEnable(){
        Main.instance = this;
        //Fired when the server enables the plugin

        this.craftbukkit = this.getCraftBukkit();
        this.minecraft = this.getMinecraftPackage();
        this.version = this.loadVersion();


        this.saveDefaultConfig();
        this.loadConfigInUTF();

        this.getConfig().options().copyDefaults(true);
        this.getConfig().options().copyHeader(true);

        ConfigUpdater configUpdater = new ConfigUpdater();
        configUpdater.addConfigs();
        this.saveConfiguration();
        this.loadConfigInUTF();

        this.loadDatabase();


        this.registerCommands();
        this.registerListener();


        this.loadStatistics();
        this.localization = this.loadLocalization();

        this.roomManager = new RoomManager();

        PluginManager manager = Bukkit.getServer().getPluginManager();
        final Plugin plotsquared = manager.getPlugin("PlotSquared");

        // Disable the plugin if PlotSquared is not installed

        // If you move any PlotSquared related tasks to en external class you
        // wouldn't have to disable the plugin if PlotSquared wasn't installed

        if(plotsquared != null && !plotsquared.isEnabled()) {
            logger.log(null, "&c[ExamplePlugin] Could not find PlotSquared! Disabling plugin...");
            manager.disablePlugin(this);
            return;
        }

        api = new PlotAPI();

    }


    @Override
    public void onDisable(){
        //Fired when the server stops and disables all plugins
        this.cleanDatabase();
    }

    private void loadDatabase() {
        if(!this.getBooleanConfig("statistics.enabled", false)
                || !this.getStringConfig("statistics.storage","yaml").equals("database")) {
            return;
        }

        this.getServer().getConsoleSender().sendMessage(ChatWriter.pluginMessage(ChatColor.GREEN + "Initialize database ..."));
        this.loadingRequiredLibs();

        String host = this.getStringConfig("database.host", null);
        int port = this.getIntConfig("database.port", 3306);
        String user = this.getStringConfig("database.user", null);
        String password = this.getStringConfig("database.password", null);
        String db = this.getStringConfig("database.db", null);

        if(host == null || user == null || password == null || db == null) {
            return;
        }

        this.dbManager = new DatabaseManager(host, port, user, password, db);
        this.dbManager.initialize();

        this.getServer().getConsoleSender().sendMessage(ChatWriter.pluginMessage(ChatColor.GREEN + "Update database ..."));
        (new DatabaseUpdater()).execute();

        this.getServer().getConsoleSender().sendMessage(ChatWriter.pluginMessage(ChatColor.GREEN + "Done."));
    }


    private void cleanDatabase() {
        if(this.dbManager != null) {
            this.dbManager.cleanUp();
        }
    }

    public int getIntConfig(String key, int defaultInt) {
        FileConfiguration config = this.getConfig();
        if(config.contains(key)) {
            if(config.isInt(key)) {
                return config.getInt(key);
            }
        }

        return defaultInt;
    }

    public String getStringConfig(String key, String defaultString) {
        FileConfiguration config = this.getConfig();
        if(config.contains(key)) {
            if(config.isString(key)) {
                return config.getString(key);
            }
        }

        return defaultString;
    }

    public boolean getBooleanConfig(String key, boolean defaultBool) {
        FileConfiguration config = this.getConfig();
        if(config.contains(key)) {
            if(config.isBoolean(key)) {
                return config.getBoolean(key);
            }
        }

        return defaultBool;
    }


    public void loadConfigInUTF() {
        File configFile = new File(this.getDataFolder(), "config.yml");
        if(!configFile.exists()) {
            return;
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(configFile), "UTF-8"));
            this.getConfig().load(reader);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(this.getConfig() == null) {
            return;
        }
    }

    public void saveConfiguration() {
        File file = new File(Main.getInstance().getDataFolder(), "config.yml");
        try {
            file.mkdirs();

            String data = this.getYamlDump((YamlConfiguration)this.getConfig());

            FileOutputStream stream = new FileOutputStream(file);

            try (OutputStreamWriter writer = new OutputStreamWriter(stream, "UTF-8")) {
                writer.write(data);
            } finally {
                stream.close();
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    public String getYamlDump(YamlConfiguration config) {
        try {
            String fullstring = config.saveToString();
            String endstring = fullstring;
            endstring = Utils.unescape_perl_string(fullstring);

            return endstring;
        } catch(Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    private void registerCommands() {

        this.commands.add(new RoomCommand(this));
        this.commands.add(new SaveMapCommand(this));

        TransformineCommandExecutor commandExecutor = new TransformineCommandExecutor(this);
        this.getCommand("room").setExecutor(commandExecutor);
        this.getCommand("save").setExecutor(commandExecutor);
    }

    private void registerListener() {
        new PlayerListener();
    }

    @SuppressWarnings("rawtypes")
    public Class getCraftBukkitClass(String classname) {
        try {
            if (this.craftbukkit == null) {
                this.craftbukkit = this.getCraftBukkit();
            }

            return Class.forName(this.craftbukkit.getName() + "." + classname);
        } catch (Exception ex) {
            this.getServer()
                    .getConsoleSender()
                    .sendMessage(
                            ChatWriter.pluginMessage(ChatColor.RED
                                    + Main._l("errors.classnotfound",
                                    ImmutableMap.of("package",
                                            "craftbukkit", "class",
                                            classname))));
            return null;
        }
    }

    @SuppressWarnings("rawtypes")
    public Class getMinecraftServerClass(String classname) {
        try {
            if (this.minecraft == null) {
                this.minecraft = this.getMinecraftPackage();
            }

            return Class.forName(this.minecraft.getName() + "." + classname);
        } catch (Exception ex) {
            this.getServer()
                    .getConsoleSender()
                    .sendMessage(
                            ChatWriter.pluginMessage(ChatColor.RED
                                    + Main._l("errors.classnotfound",
                                    ImmutableMap.of("package",
                                            "minecraft server",
                                            "class", classname))));
            return null;
        }
    }

    public Package getCraftBukkit() {
        try {
            if (this.craftbukkit == null) {
                return Package.getPackage("org.bukkit.craftbukkit."
                        + Bukkit.getServer().getClass().getPackage().getName()
                        .replace(".", ",").split(",")[3]);
            } else {
                return this.craftbukkit;
            }
        } catch (Exception ex) {
            this.getServer()
                    .getConsoleSender()
                    .sendMessage(
                            ChatWriter.pluginMessage(ChatColor.RED
                                    + Main._l("errors.packagenotfound",
                                    ImmutableMap.of("package",
                                            "craftbukkit"))));
            return null;
        }
    }

    public Package getMinecraftPackage() {
        try {
            if (this.minecraft == null) {
                return Package.getPackage("net.minecraft.server."
                        + Bukkit.getServer().getClass().getPackage().getName()
                        .replace(".", ",").split(",")[3]);
            } else {
                return this.minecraft;
            }
        } catch (Exception ex) {
            this.getServer()
                    .getConsoleSender()
                    .sendMessage(
                            ChatWriter.pluginMessage(ChatColor.RED
                                    + Main._l("errors.packagenotfound",
                                    ImmutableMap.of("package",
                                            "minecraft server"))));
            return null;
        }
    }

    public static String _l(String localeKey, Map<String, String> params) {
        return (String) Main.getInstance().getLocalization()
                .get(localeKey, params);
    }

    public static String _l(String localeKey) {
        return (String) Main.getInstance().getLocalization().get(localeKey);
    }

    private LocalizationConfig loadLocalization() {
        LocalizationConfig config = new LocalizationConfig();
        config.saveLocales(false);

        config.loadLocale(this.getConfig().getString("locale"), false);
        return config;
    }

    public LocalizationConfig getLocalization() {
        return this.localization;
    }

    public String getFallbackLocale() {
        return "ru";
    }

    private String loadVersion() {
        String packName = Bukkit.getServer().getClass().getPackage().getName();
        return packName.substring(packName.lastIndexOf('.') + 1);
    }


    private void loadingRequiredLibs() {
        try {
            final File[] libs = new File[] {
                    new File(this.getDataFolder() + "/lib/", "c3p0-0.9.5.jar"),
                    new File(this.getDataFolder() + "/lib/", "mchange-commons-java-0.2.9.jar")};
            for (final File lib : libs) {
                if (!lib.exists()) {
                    JarUtils.extractFromJar(lib.getName(),
                            lib.getAbsolutePath());
                }
            }
            for (final File lib : libs) {
                if (!lib.exists()) {
                    this.getLogger().warning(
                            "There was a critical error loading bedwars plugin! Could not find lib: "
                                    + lib.getName());
                    Bukkit.getServer().getPluginManager().disablePlugin(this);
                    return;
                }
                this.addClassPath(JarUtils.getJarUrl(lib));
            }
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    private void addClassPath(final URL url) throws IOException {
        final URLClassLoader sysloader = (URLClassLoader) ClassLoader.getSystemClassLoader();
        final Class<URLClassLoader> sysclass = URLClassLoader.class;
        try {
            final Method method = sysclass.getDeclaredMethod("addURL", URL.class);
            method.setAccessible(true);
            method.invoke(sysloader, url);
        } catch (final Throwable t) {
            t.printStackTrace();
            throw new IOException("Error adding " + url + " to system classloader");
        }
    }

    public String getCurrentVersion() {
        return this.version;
    }

    public PlayerStatisticManager getPlayerStatisticManager() {
        return this.playerStatisticManager;
    }

    private void loadStatistics() {
        this.playerStatisticManager = new PlayerStatisticManager();
        this.playerStatisticManager.initialize();
    }

    public StorageType getStatisticStorageType() {
        String storage = this.getStringConfig("statistics.storage", "yaml");
        return StorageType.getByName(storage);
    }

    public DatabaseManager getDatabaseManager() {
        return this.dbManager;
    }

    public static Main getInstance() {
        return instance;
    }

    public RoomManager getRoomManager() {

        return roomManager;
    }

    public ArrayList<BaseCommand> getCommands() {
        return this.commands;
    }
}