/*
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                     Version 2, December 2004
 *
 *  Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 *  Everyone is permitted to copy and distribute verbatim or modified
 *  copies of this license document, and changing it is allowed as long
 *  as the name is changed.
 *
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *   0. You just DO WHAT THE FUCK YOU WANT TO.
 *
 */

package index.transformine.Database;

public abstract class DatabaseObject {
	
	private long id = 0;
	
	public DatabaseObject() {
		this.id = 0;
	}

	@DBGetField(name = "id", dbType = "INT(10) UNSIGNED", autoInc = true)
	public long getId() {
		return this.id;
	}

	@DBSetField(name = "id")
	public void setId(long id) {
		this.id = id;
	}
	
	public boolean isNew() {
		return (this.id == 0);
	}
	
}