/*
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                     Version 2, December 2004
 *
 *  Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 *  Everyone is permitted to copy and distribute verbatim or modified
 *  copies of this license document, and changing it is allowed as long
 *  as the name is changed.
 *
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *   0. You just DO WHAT THE FUCK YOU WANT TO.
 *
 */

package index.transformine.Commands

import index.transformine.ChatWriter
import index.transformine.Main
import org.bukkit.ChatColor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import java.util.*

/**
 * Created by ksnzk on 03.03.2016.
 */
abstract class BaseCommand(plugin: Main) {

    protected val plugin: Main;

    init {
        this.plugin = plugin
    }

    abstract val command: String

    abstract val name: String

    abstract val description: String

    abstract val arguments: Array<String>

    abstract val permission: String

    abstract fun execute(sender: CommandSender, args: ArrayList<String>): Boolean

    fun hasPermission(sender: CommandSender): Boolean {
        if (sender !is Player) {
            sender.sendMessage(ChatWriter.pluginMessage("Only players should execute this command!"))
            return false
        }

        if (!sender.hasPermission("bw." + this.permission)) {
            sender.sendMessage(ChatWriter.pluginMessage(ChatColor.RED.toString() + "You don't have permission to execute this command!"))
            return false
        }

        return true
    }

}
