/*
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                     Version 2, December 2004
 *
 *  Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 *  Everyone is permitted to copy and distribute verbatim or modified
 *  copies of this license document, and changing it is allowed as long
 *  as the name is changed.
 *
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *   0. You just DO WHAT THE FUCK YOU WANT TO.
 *
 */

package index.transformine.Commands

import com.google.common.collect.ImmutableMap
import index.transformine.ChatWriter
import index.transformine.Main
import org.bukkit.ChatColor
import org.bukkit.command.CommandSender
import java.util.*

/**
 * Created by ksnzk on 03.03.2016.
 */
class SaveMapCommand(plugin: Main) : BaseCommand(plugin) {

    override val permission: String
        get() = "setup"

    override val command: String
        get() = "save"

    override val name: String
        get() = Main._l("commands.save.name")

    override val description: String
        get() = Main._l("commands.save.desc")

    override val arguments: Array<String>
        get() = arrayOf("mode")

    override fun execute(sender: CommandSender, args: ArrayList<String>): Boolean {
        if (this.hasPermission(sender)) {
            return false
        }


        sender.sendMessage(ChatWriter.pluginMessage(ChatColor.GREEN.toString() + Main._l("success.saved")))
        return true
    }

}
