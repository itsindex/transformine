/*
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                     Version 2, December 2004
 *
 *  Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 *  Everyone is permitted to copy and distribute verbatim or modified
 *  copies of this license document, and changing it is allowed as long
 *  as the name is changed.
 *
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *   0. You just DO WHAT THE FUCK YOU WANT TO.
 *
 */

package index.transformine;

import net.minecraft.server.v1_8_R3.NBTTagCompound;
import net.minecraft.server.v1_8_R3.NBTTagList;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.MemorySection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * Created by ksnzk on 24.02.2016.
 */
public class Utils {

    public static String implode(String glue, ArrayList<String> strings) {
        if (strings.isEmpty()) {
            return "";
        }

        StringBuilder builder = new StringBuilder();
        builder.append(strings.remove(0));

        for (String str : strings) {
            builder.append(glue);
            builder.append(str);
        }

        return builder.toString();
    }

    public static Location getDirectionLocation(Location location,
                                                int blockOffset) {
        Location loc = location.clone();
        return loc.add(loc.getDirection().setY(0).normalize()
                .multiply(blockOffset));
    }

    public static Object getCraftPlayer(Player player) {
        try {
            Class<?> craftPlayerClass = Main.getInstance().getCraftBukkitClass(
                    "entity.CraftPlayer");
            Method getHandle = craftPlayerClass.getMethod("getHandle"
            );
            getHandle.setAccessible(true);

            return getHandle.invoke(player);
        } catch (Exception e) {
            return null;
        }
    }

    public static boolean isNumber(String numberString) {
        try {
            Integer.parseInt(numberString);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public static Method getColorableMethod(Material mat) {
        try {
            ItemStack tempStack = new ItemStack(mat, 1);
            Method method = tempStack.getItemMeta().getClass()
                    .getMethod("setColor", Color.class);
            if (method != null) {
                return method;
            }
        } catch (Exception ex) {
            // it's no error
        }

        return null;
    }

    public static boolean checkBungeePlugin() {
        try {
            Class.forName("net.md_5.bungee.BungeeCord");
            return true;
        } catch (Exception ignored) {
        }

        return false;
    }

    public static int randInt(int min, int max) {
        Random rand = new Random();

        return rand.nextInt((max - min) + 1) + min;
    }

    public static Map<String, Object> locationSerialize(Location location) {
        Map<String, Object> section = new HashMap<>();
        section.put("x", location.getX());
        section.put("y", location.getY());
        section.put("z", location.getZ());
        section.put("pitch", (double) location.getPitch());
        section.put("yaw", (double) location.getYaw());
        section.put("world", location.getWorld().getName());

        return section;
    }

    @SuppressWarnings("unchecked")
    public static Location locationDeserialize(Object obj) {
        if (obj instanceof Location) {
            return (Location) obj;
        }

        Map<String, Object> section = new HashMap<>();
        if (obj instanceof MemorySection) {
            MemorySection sec = (MemorySection) obj;
            for (String key : sec.getKeys(false)) {
                section.put(key, sec.get(key));
            }
        } else if (obj instanceof ConfigurationSection) {
            ConfigurationSection sec = (ConfigurationSection) obj;
            for (String key : sec.getKeys(false)) {
                section.put(key, sec.get(key));
            }
        } else {
            section = (Map<String, Object>) obj;
        }

        try {
            if (section == null) {
                return null;
            }

            double x = Double.valueOf(section.get("x").toString());
            double y = Double.valueOf(section.get("y").toString());
            double z = Double.valueOf(section.get("z").toString());
            float yaw = Float.valueOf(section.get("yaw").toString());
            float pitch = Float.valueOf(section.get("pitch").toString());
            World world = Main.getInstance().getServer()
                    .getWorld(section.get("world").toString());

            if (world == null) {
                return null;
            }

            return new Location(world, x, y, z, yaw, pitch);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public static Location locationDeserialize(String key,
                                               FileConfiguration config) {
        if (!config.contains(key)) {
            return null;
        }

        Object locSec = config.get(key);
        if (locSec instanceof Location) {
            return (Location) locSec;
        }

        try {
            Map<String, Object> section = (Map<String, Object>) config.get(key);
            if (section == null) {
                return null;
            }

            double x = Double.valueOf(section.get("x").toString());
            double y = Double.valueOf(section.get("y").toString());
            double z = Double.valueOf(section.get("z").toString());
            float yaw = Float.valueOf(section.get("yaw").toString());
            float pitch = Float.valueOf(section.get("pitch").toString());
            World world = Main.getInstance().getServer()
                    .getWorld(section.get("world").toString());

            if (world == null) {
                return null;
            }

            return new Location(world, x, y, z, yaw, pitch);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public static Class<?> getPrimitiveWrapper(Class<?> primitive) {
        if (!primitive.isPrimitive()) {
            return primitive;
        }

        if (primitive.getSimpleName().equals("int")) {
            return Integer.class;
        } else if (primitive.getSimpleName().equals("long")) {
            return Long.class;
        } else if (primitive.getSimpleName().equals("short")) {
            return Short.class;
        } else if (primitive.getSimpleName().equals("byte")) {
            return Byte.class;
        } else if (primitive.getSimpleName().equals("float")) {
            return Float.class;
        } else if (primitive.getSimpleName().equals("boolean")) {
            return Boolean.class;
        } else if (primitive.getSimpleName().equals("char")) {
            return Character.class;
        } else if (primitive.getSimpleName().equals("double")) {
            return Double.class;
        } else {
            return primitive;
        }
    }

    public static Class<?> getGenericTypeOfParameter(Class<?> clazz,
                                                     String method, int parameterIndex) {
        try {
            Method m = clazz.getMethod(method, Set.class,
                    int.class);
            ParameterizedType type = (ParameterizedType) m
                    .getGenericParameterTypes()[parameterIndex];
            return (Class<?>) type.getActualTypeArguments()[0];
        } catch (Exception e) {
            try {
                Method m = clazz.getMethod(method, HashSet.class, int.class);
                ParameterizedType type = (ParameterizedType) m
                        .getGenericParameterTypes()[parameterIndex];
                return (Class<?>) type.getActualTypeArguments()[0];
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        return null;
    }

    public static String unescape_perl_string(String oldstr) {

		/*
		 * In contrast to fixing Java's broken regex charclasses, this one need
		 * be no bigger, as unescaping shrinks the string here, where in the
		 * other one, it grows it.
		 */

        StringBuffer newstr = new StringBuffer(oldstr.length());

        boolean saw_backslash = false;

        for (int i = 0; i < oldstr.length(); i++) {
            int cp = oldstr.codePointAt(i);
            if (oldstr.codePointAt(i) > Character.MAX_VALUE) {
                i++;
                /**** WE HATES UTF-16! WE HATES IT FOREVERSES!!! ****/
            }

            if (!saw_backslash) {
                if (cp == '\\') {
                    saw_backslash = true;
                } else {
                    newstr.append(Character.toChars(cp));
                }
                continue; /* switch */
            }

            if (cp == '\\') {
                saw_backslash = false;
                newstr.append('\\');
                newstr.append('\\');
                continue; /* switch */
            }

            switch (cp) {

                case 'r':
                    newstr.append('\r');
                    break; /* switch */

                case 'n':
                    newstr.append('\n');
                    break; /* switch */

                case 'f':
                    newstr.append('\f');
                    break; /* switch */

			/* PASS a \b THROUGH!! */
                case 'b':
                    newstr.append("\\b");
                    break; /* switch */

                case 't':
                    newstr.append('\t');
                    break; /* switch */

                case 'a':
                    newstr.append('\007');
                    break; /* switch */

                case 'e':
                    newstr.append('\033');
                    break; /* switch */

			/*
			 * A "control" character is what you get when you xor its codepoint
			 * with '@'==64. This only makes sense for ASCII, and may not yield
			 * a "control" character after all.
			 *
			 * Strange but true: "\c{" is ";", "\c}" is "=", etc.
			 */
                case 'c': {
                    if (++i == oldstr.length()) {
                        die("trailing \\c");
                    }
                    cp = oldstr.codePointAt(i);
				/*
				 * don't need to grok surrogates, as next line blows them up
				 */
                    if (cp > 0x7f) {
                        die("expected ASCII after \\c");
                    }
                    newstr.append(Character.toChars(cp ^ 64));
                    break; /* switch */
                }

                case '8':
                case '9':
                    die("illegal octal digit");
				/* NOTREACHED */

				/*
				 * may be 0 to 2 octal digits following this one so back up one
				 * for fallthrough to next case; unread this digit and fall
				 * through to next case.
				 */
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                    --i;
				/* FALLTHROUGH */

				/*
				 * Can have 0, 1, or 2 octal digits following a 0 this permits
				 * larger values than octal 377, up to octal 777.
				 */
                case '0': {
                    if (i + 1 == oldstr.length()) {
					/* found \0 at end of string */
                        newstr.append(Character.toChars(0));
                        break; /* switch */
                    }
                    i++;
                    int digits = 0;
                    int j;
                    for (j = 0; j <= 2; j++) {
                        if (i + j == oldstr.length()) {
                            break; /* for */
                        }
					/* safe because will unread surrogate */
                        int ch = oldstr.charAt(i + j);
                        if (ch < '0' || ch > '7') {
                            break; /* for */
                        }
                        digits++;
                    }
                    if (digits == 0) {
                        --i;
                        newstr.append('\0');
                        break; /* switch */
                    }
                    int value = 0;
                    try {
                        value = Integer
                                .parseInt(oldstr.substring(i, i + digits), 8);
                    } catch (NumberFormatException nfe) {
                        die("invalid octal value for \\0 escape");
                    }
                    newstr.append(Character.toChars(value));
                    i += digits - 1;
                    break; /* switch */
                } /* end case '0' */

                case 'x': {
                    if (i + 2 > oldstr.length()) {
                        die("string too short for \\x escape");
                    }
                    i++;
                    boolean saw_brace = false;
                    if (oldstr.charAt(i) == '{') {
					/* ^^^^^^ ok to ignore surrogates here */
                        i++;
                        saw_brace = true;
                    }
                    int j;
                    for (j = 0; j < 8; j++) {

                        if (!saw_brace && j == 2) {
                            break; /* for */
                        }

					/*
					 * ASCII test also catches surrogates
					 */
                        int ch = oldstr.charAt(i + j);
                        if (ch > 127) {
                            die("illegal non-ASCII hex digit in \\x escape");
                        }

                        if (saw_brace && ch == '}') {
                            break; /* for */
                        }

                        if (!((ch >= '0' && ch <= '9') || (ch >= 'a' && ch <= 'f') || (ch >= 'A' && ch <= 'F'))) {
                            die(String.format("illegal hex digit #%d '%c' in \\x",
                                    ch, ch));
                        }

                    }
                    if (j == 0) {
                        die("empty braces in \\x{} escape");
                    }
                    int value = 0;
                    try {
                        value = Integer.parseInt(oldstr.substring(i, i + j), 16);
                    } catch (NumberFormatException nfe) {
                        die("invalid hex value for \\x escape");
                    }
                    newstr.append(Character.toChars(value));
                    if (saw_brace) {
                        j++;
                    }
                    i += j - 1;
                    break; /* switch */
                }

                case 'u': {
                    if (i + 4 > oldstr.length()) {
                        die("string too short for \\u escape");
                    }
                    i++;
                    int j;
                    for (j = 0; j < 4; j++) {
					/* this also handles the surrogate issue */
                        if (oldstr.charAt(i + j) > 127) {
                            die("illegal non-ASCII hex digit in \\u escape");
                        }
                    }
                    int value = 0;
                    try {
                        value = Integer.parseInt(oldstr.substring(i, i + j), 16);
                    } catch (NumberFormatException nfe) {
                        die("invalid hex value for \\u escape");
                    }
                    newstr.append(Character.toChars(value));
                    i += j - 1;
                    break; /* switch */
                }

                case 'U': {
                    if (i + 8 > oldstr.length()) {
                        die("string too short for \\U escape");
                    }
                    i++;
                    int j;
                    for (j = 0; j < 8; j++) {
					/* this also handles the surrogate issue */
                        if (oldstr.charAt(i + j) > 127) {
                            die("illegal non-ASCII hex digit in \\U escape");
                        }
                    }
                    int value = 0;
                    try {
                        value = Integer.parseInt(oldstr.substring(i, i + j), 16);
                    } catch (NumberFormatException nfe) {
                        die("invalid hex value for \\U escape");
                    }
                    newstr.append(Character.toChars(value));
                    i += j - 1;
                    break; /* switch */
                }

                default:
                    newstr.append('\\');
                    newstr.append(Character.toChars(cp));
				/*
				 * say(String.format(
				 * "DEFAULT unrecognized escape %c passed through", cp));
				 */
                    break; /* switch */

            }
            saw_backslash = false;
        }

		/* weird to leave one at the end */
        if (saw_backslash) {
            newstr.append('\\');
        }

        return newstr.toString();
    }

    private static void die(String foa) {
        throw new IllegalArgumentException(foa);
    }

    /*
     * Return a string "U+XX.XXX.XXXX" etc, where each XX set is the xdigits of
     * the logical Unicode code point. No bloody brain-damaged UTF-16 surrogate
     * crap, just true logical characters.
     */
    public static String uniplus(String s) {
        if (s.length() == 0) {
            return "";
        }
		/* This is just the minimum; sb will grow as needed. */
        StringBuffer sb = new StringBuffer(2 + 3 * s.length());
        sb.append("U+");
        for (int i = 0; i < s.length(); i++) {
            sb.append(String.format("%X", s.codePointAt(i)));
            if (s.codePointAt(i) > Character.MAX_VALUE) {
                i++;
                /**** WE HATES UTF-16! WE HATES IT FOREVERSES!!! ****/
            }
            if (i + 1 < s.length()) {
                sb.append(".");
            }
        }
        return sb.toString();
    }

    public static BlockFace getCardinalDirection(Location location) {
        double rotation = (location.getYaw() - 90) % 360;
        if (rotation < 0) {
            rotation += 360.0;
        }
        if (0 <= rotation && rotation < 22.5) {
            return BlockFace.NORTH;
        } else if (22.5 <= rotation && rotation < 67.5) {
            return BlockFace.NORTH_EAST;
        } else if (67.5 <= rotation && rotation < 112.5) {
            return BlockFace.EAST;
        } else if (112.5 <= rotation && rotation < 157.5) {
            return BlockFace.SOUTH_EAST;
        } else if (157.5 <= rotation && rotation < 202.5) {
            return BlockFace.SOUTH;
        } else if (202.5 <= rotation && rotation < 247.5) {
            return BlockFace.SOUTH_WEST;
        } else if (247.5 <= rotation && rotation < 292.5) {
            return BlockFace.WEST;
        } else if (292.5 <= rotation && rotation < 337.5) {
            return BlockFace.NORTH_WEST;
        } else if (337.5 <= rotation && rotation < 360.0) {
            return BlockFace.NORTH;
        } else {
            return BlockFace.NORTH;
        }
    }

    public static boolean isSupportingTitles() {
        try {
            Class.forName("io.github.yannici.bedwars.Com."
                    + Main.getInstance().getCurrentVersion() + ".Title");
            return true;
        } catch (Exception ex) {
            // no support
        }

        return false;
    }

    public static String getFormattedTime(int time) {
        int hr = 0;
        int min = 0;
        int sec = 0;
        String minStr = "";
        String secStr = "";
        String hrStr = "";

        hr = (int) Math.floor((time / 60) / 60);
        min = ((int) Math.floor((time / 60)) - (hr * 60));
        sec = time % 60;

        hrStr = (hr < 10) ? "0" + String.valueOf(hr) : String.valueOf(hr);
        minStr = (min < 10) ? "0" + String.valueOf(min) : String.valueOf(min);
        secStr = (sec < 10) ? "0" + String.valueOf(sec) : String.valueOf(sec);

        return hrStr + ":" + minStr + ":" + secStr;
    }

    @SuppressWarnings("deprecation")
    public static Material parseMaterial(String material) {
        try {
            if(Utils.isNumber(material)) {
                return Material.getMaterial(Integer.parseInt(material));
            } else {
                return Material.getMaterial(material.toUpperCase());
            }
        } catch(Exception ex) {
            // failed to parse
        }

        return null;
    }


    public static void sendActionBar(Player player, String message){
        try {
            String nmsver = "v1_8_R3";
            Class<?> c1 = Class.forName("org.bukkit.craftbukkit." + nmsver + ".entity.CraftPlayer");
            Object p = c1.cast(player);
            Object ppoc = null;
            Class<?> c4 = Class.forName("net.minecraft.server." + nmsver + ".PacketPlayOutChat");
            Class<?> c5 = Class.forName("net.minecraft.server." + nmsver + ".Packet");
            if (nmsver.equalsIgnoreCase("v1_8_R1") || !nmsver.startsWith("v1_8_")) {
                Class<?> c2 = Class.forName("net.minecraft.server." + nmsver + ".ChatSerializer");
                Class<?> c3 = Class.forName("net.minecraft.server." + nmsver + ".IChatBaseComponent");
                Method m3 = c2.getDeclaredMethod("a", String.class);
                Object cbc = c3.cast(m3.invoke(c2, "{\"text\": \"" + message + "\"}"));
                ppoc = c4.getConstructor(new Class<?>[] {c3, byte.class}).newInstance(cbc, (byte) 2);
            } else {
                Class<?> c2 = Class.forName("net.minecraft.server." + nmsver + ".ChatComponentText");
                Class<?> c3 = Class.forName("net.minecraft.server." + nmsver + ".IChatBaseComponent");
                Object o = c2.getConstructor(new Class<?>[] {String.class}).newInstance(message);
                ppoc = c4.getConstructor(new Class<?>[] {c3, byte.class}).newInstance(o, (byte) 2);
            }
            Method m1 = c1.getDeclaredMethod("getHandle");
            Object h = m1.invoke(p);
            Field f1 = h.getClass().getDeclaredField("playerConnection");
            Object pc = f1.get(h);
            Method m5 = pc.getClass().getDeclaredMethod("sendPacket", c5);
            m5.invoke(pc, ppoc);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @SuppressWarnings("resource")
    public static String[] getResourceListing(Class<?> clazz, String path)
            throws URISyntaxException, IOException {
        URL dirURL = clazz.getClassLoader().getResource(path);
        if (dirURL != null && dirURL.getProtocol().equals("file")) {
			/* A file path: easy enough */
            return new File(dirURL.toURI()).list();
        }

        if (dirURL == null) {
			/*
			 * In case of a jar file, we can't actually find a directory. Have
			 * to assume the same jar as clazz.
			 */
            String me = clazz.getName().replace(".", "/") + ".class";
            dirURL = clazz.getClassLoader().getResource(me);
        }

        if (dirURL.getProtocol().equals("jar")) {
			/* A JAR path */
            String jarPath = dirURL.getPath().substring(5,
                    dirURL.getPath().indexOf("!")); // strip out only the JAR
            // file
            JarFile jar = new JarFile(URLDecoder.decode(jarPath, "UTF-8"));
            Enumeration<JarEntry> entries = jar.entries(); // gives ALL entries
            // in jar
            Set<String> result = new HashSet<>(); // avoid duplicates in
            // case it is a
            // subdirectory
            while (entries.hasMoreElements()) {
                String name = entries.nextElement().getName();
                if (name.startsWith(path)) { // filter according to the path
                    String entry = name.substring(path.length());
                    int checkSubdir = entry.indexOf("/");
                    if (checkSubdir >= 0) {
                        // if it is a subdirectory, we just return the directory
                        // name
                        entry = entry.substring(0, checkSubdir);
                    }
                    result.add(entry);
                }
            }
            return result.toArray(new String[result.size()]);
        }

        throw new UnsupportedOperationException("Cannot list files for URL "
                + dirURL);
    }

    public static ItemStack addGlow(ItemStack item) {
        net.minecraft.server.v1_8_R3.ItemStack nmsStack = CraftItemStack.asNMSCopy(item);
        NBTTagCompound tag = null;
        if (!nmsStack.hasTag()) {
            tag = new NBTTagCompound();
            nmsStack.setTag(tag);
        }
        if (tag == null) tag = nmsStack.getTag();
        NBTTagList ench = new NBTTagList();
        tag.set("ench", ench);
        nmsStack.setTag(tag);
        return CraftItemStack.asCraftMirror(nmsStack);
    }
}
