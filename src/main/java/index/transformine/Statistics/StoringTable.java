/*
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                     Version 2, December 2004
 *
 *  Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 *  Everyone is permitted to copy and distribute verbatim or modified
 *  copies of this license document, and changing it is allowed as long
 *  as the name is changed.
 *
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *   0. You just DO WHAT THE FUCK YOU WANT TO.
 *
 */

package index.transformine.Statistics;


import index.transformine.ChatWriter;
import index.transformine.Database.DBField;
import index.transformine.Database.DBGetField;
import index.transformine.Database.DBSetField;
import index.transformine.Database.DatabaseObject;
import index.transformine.Main;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;


public abstract class StoringTable extends DatabaseObject {

	private Map<String, DBField> fields = null;

	public StoringTable() {
		this.fields = new HashMap<>();
		this.loadFields();
	}

	private void loadFields() {
		this.fields.clear();

		for(Method method : this.getClass().getMethods()) {
			DBGetField getAnnotation = method.getAnnotation(DBGetField.class);
			DBSetField setAnnotation = method.getAnnotation(DBSetField.class);

			if(getAnnotation == null && setAnnotation == null) {
				continue;
			}

			String fieldName = (getAnnotation != null) ? getAnnotation.name() : setAnnotation.name();

			if(this.fields.containsKey(fieldName)) {
				DBField field = this.fields.get(fieldName);

				if(getAnnotation == null) {
					field.setSetter(method);
				} else {
					field.setGetter(method);
				}
			} else {
				DBField field = new DBField();

				if(getAnnotation == null) {
					field.setSetter(method);
				} else {
					field.setGetter(method);
				}

				this.fields.put(fieldName, field);
			}
		}
	}

	public Map<String, DBField> getFields() {
		return this.fields;
	}

	public abstract String getTableName();

	public abstract String getKeyField();

	public abstract void load();

	public abstract void store();

	public abstract void setDefault();

	public Object getValue(String field) {
		try {
			Method getter = this.fields.get(field).getGetter();

			getter.setAccessible(true);
			return getter.invoke(this);
		} catch(Exception ex) {
			Main.getInstance().getServer().getConsoleSender().sendMessage(ChatWriter.pluginMessage("Couldn't fetch value of field: " + field));
		}

		return null;
	}

	public void setValue(String field, Object value) {
		try {
			Method setter = this.fields.get(field).getSetter();

			if(setter == null) {
				return;
			}

			setter.setAccessible(true);

			Class<?> paramType = setter.getParameterTypes()[0];

			try {
				if(value instanceof Number) {
					String classname = value.getClass().getSimpleName().toLowerCase();
					if(value.getClass().equals(Integer.class)) {
						classname = "int";
					}

					Method castNumber = value.getClass().getMethod(classname + "Value");
					castNumber.setAccessible(true);

					Class<?> returningNumberType = castNumber.getReturnType();
					if(!paramType.equals(returningNumberType)) {
						setter.invoke(this, paramType.cast(castNumber.invoke(value)));
						return;
					}

					setter.invoke(this, castNumber.invoke(value));
					return;
				}

				value = paramType.cast(value);
				setter.invoke(this, value);
			} catch(Exception ex) {
				Main.getInstance().getServer().getConsoleSender().sendMessage(ChatWriter.pluginMessage("Couldn't cast value for field: " + field));
			}
		} catch(Exception ex) {
			Main.getInstance().getServer().getConsoleSender().sendMessage(ChatWriter.pluginMessage("Couldn't set value of field: " + field));
		}
	}
}