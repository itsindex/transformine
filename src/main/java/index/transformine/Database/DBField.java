/*
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                     Version 2, December 2004
 *
 *  Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 *  Everyone is permitted to copy and distribute verbatim or modified
 *  copies of this license document, and changing it is allowed as long
 *  as the name is changed.
 *
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *   0. You just DO WHAT THE FUCK YOU WANT TO.
 *
 */

package index.transformine.Database;

import java.lang.reflect.Method;

public class DBField {
	
	private Method getter = null;
	private Method setter = null;
	
	public DBField() {
		this.getter = null;
		this.setter = null;
	}
	
	public DBField(Method getter, Method setter) {
		this.getter = getter;
		this.setter = setter;
	}
	
	public Method getGetter() {
		return this.getter;
	}
	
	public Method getSetter() {
		return this.setter;
	}

	public void setGetter(Method getter) {
		this.getter = getter;
	}

	public void setSetter(Method setter) {
		this.setter = setter;
	}

}
