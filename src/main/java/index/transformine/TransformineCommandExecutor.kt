/*
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                     Version 2, December 2004
 *
 *  Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 *  Everyone is permitted to copy and distribute verbatim or modified
 *  copies of this license document, and changing it is allowed as long
 *  as the name is changed.
 *
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *   0. You just DO WHAT THE FUCK YOU WANT TO.
 *
 */

package index.transformine

import org.bukkit.ChatColor
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import java.util.*

/**
 * Created by ksnzk on 03.03.2016.
 */
class TransformineCommandExecutor(plugin: Main) : CommandExecutor {

    private val plugin: Main;

    init {

        this.plugin = plugin
    }

    override fun onCommand(sender: CommandSender, cmd: Command,
                           commandLabel: String, args: Array<String>): Boolean {

        val arguments = ArrayList(Arrays.asList(*args))

        for (bCommand in this.plugin.getCommands()) {
            if (bCommand.command.equals(commandLabel,true)) {
                if (bCommand.arguments.size > arguments.size) {
                    sender.sendMessage(ChatWriter.pluginMessage(ChatColor.RED.toString() + Main._l("errors.argumentslength")))
                    return false
                }

                return bCommand.execute(sender, arguments)
            }
        }

        return false
    }

}
