/*
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                     Version 2, December 2004
 *
 *  Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 *  Everyone is permitted to copy and distribute verbatim or modified
 *  copies of this license document, and changing it is allowed as long
 *  as the name is changed.
 *
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *   0. You just DO WHAT THE FUCK YOU WANT TO.
 *
 */

package index.transformine.Game;

import com.intellectualcrafters.plot.flag.AbstractFlag;
import com.intellectualcrafters.plot.flag.Flag;
import com.intellectualcrafters.plot.flag.FlagManager;
import com.intellectualcrafters.plot.object.Plot;
import com.intellectualcrafters.plot.util.MainUtil;
import com.intellectualcrafters.plot.util.SetBlockQueue;
import com.intellectualcrafters.plot.util.TaskManager;
import index.transformine.Main;
import net.minecraft.server.v1_8_R3.Scoreboard;
import net.minecraft.server.v1_8_R3.ScoreboardTeam;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.scoreboard.ScoreboardManager;
import org.bukkit.scoreboard.Team;
import org.omg.IOP.ExceptionDetailMessage;

import java.util.List;

/**
 * Created by ksnzk on 24.02.2016.
 */
public class Room {

    private String name = null;
    Team roomTeam = null;
    List<Player> players = null;
    Location location = null;
    Plot plot = null;
    int time;
    RoomMode mode = null;

    Room(RoomMode mode, String name) throws InterruptedException {
        this.name = name;
        this.mode = mode;
        this.loadPlot();
        if (this.plot == null) {
            throw new InterruptedException();
        }
        ScoreboardManager manager = Bukkit.getScoreboardManager();
        this.roomTeam = manager.getMainScoreboard().registerNewTeam(name);
    }

    private void loadPlot(){
        this.plot = Main.getInstance().getRoomManager().getFreePlot();
    }

    public String getName() {
        return name;
    }

    //игрок вышел из игры, сменил комнату, или вернулся в лобби
    public void onPlayerLeaveRoom(Player player) {
        roomTeam.removePlayer(player);
        players.remove(player);

        //если в комнате 0 игроков, удаляем ее
        if(players.isEmpty()){
            this.deleteRoom();
        }
    }

    private void toNextMap() {
        final RoomManager manager = Main.getInstance().getRoomManager();
        Plot thisPlot = plot;
        this.plot = manager.getNextMap(mode);
        manager.addFreePlot(thisPlot);
    }

    private void spawnPlayers() {
        if (FlagManager.getPlotFlagRaw(plot, "spawn-location") != null) {
            this.location = (Location) FlagManager.getPlotFlagRaw(plot, "spawn-location").getValue();
        } else {
            this.location = (Location) plot.getHome().toBukkitLocation();
        }
        players.stream().forEach(player -> player.teleport(location, PlayerTeleportEvent.TeleportCause.PLUGIN));
    }

    private void deleteRoom() {
        final RoomManager manager = Main.getInstance().getRoomManager();
        roomTeam.unregister();;
        manager.unregisterRoom(this);
        if(!this.players.isEmpty()) {
            //manager.sendToFreeRoom(players,mode);
        }
    }

    public Plot getPlot() {
        return plot;
    }

}
