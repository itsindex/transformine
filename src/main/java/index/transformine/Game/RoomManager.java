/*
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                     Version 2, December 2004
 *
 *  Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 *  Everyone is permitted to copy and distribute verbatim or modified
 *  copies of this license document, and changing it is allowed as long
 *  as the name is changed.
 *
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *   0. You just DO WHAT THE FUCK YOU WANT TO.
 *
 */

package index.transformine.Game;

import com.intellectualcrafters.plot.flag.Flag;
import com.intellectualcrafters.plot.flag.FlagManager;
import com.intellectualcrafters.plot.object.Plot;
import com.intellectualcrafters.plot.object.RunnableVal;
import com.intellectualcrafters.plot.util.MainUtil;
import com.intellectualcrafters.plot.util.SchematicHandler;
import com.intellectualcrafters.plot.util.SetBlockQueue;
import com.intellectualcrafters.plot.util.TaskManager;
import index.transformine.Main;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by ksnzk on 24.02.2016.
 */
public class RoomManager {

    private ArrayList<Room> rooms = null;
    private Map<Player, Room> roomPlayer = null;
    private LinkedList<Plot> freePlots = null;

    public RoomManager () {
        this.rooms = new ArrayList<>();
        this.freePlots = new LinkedList<>();
        this.roomPlayer = new HashMap<>();
    }

    private void initFreePlots() {
        final Main main = Main.getInstance();
        ConfigurationSection section = main.getConfig().getConfigurationSection("world-settings.free-plots");
        for (String world : section.getKeys(false)) {
            final World w = main.getServer().getWorld(world);
            for(String id : section.getStringList(world)) {
                freePlots.add(Plot.fromString(world,id));
            };
        }
    }

    public Room addRoom (String name) {
        RoomMode mode = getMode(name);
        return null;
    }

    private RoomMode getMode(String name) {
        if(name.contains("vanila")) {
            return RoomMode.VANILA;
        }
        else return RoomMode.NORMAL;
    }

    public Room getRoom(String name) throws InterruptedException {
        Optional<Room> anyRoom = rooms.stream().filter(room -> room.getName().equals(name)).findAny();
        if ( anyRoom.isPresent() ) {
            return anyRoom.get();
        }
        else {
            return new Room(RoomMode.getModeByName(name), name);
        }
    }

    public ArrayList<Room> getRooms() {
        return this.rooms;
    }


    public Room getRoomOfPlayer(Player p) {
        return null;
    }

    public Plot getNextMap(RoomMode mode) {

        Plot plot = freePlots.stream().findAny().get();
        Path schem = null;
        try {
            schem = Files.walk(Paths.get(Main.getInstance().getDataFolder().getPath()
                    + "/schematics/" + mode.toString())).filter(Files::isRegularFile).findAny().get();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        final File schematicFile = schem.toFile();
        TaskManager.runTaskAsync(new Runnable() {
            @Override
            public void run() {
                SchematicHandler.Schematic schematic = SchematicHandler.manager.getSchematic(schematicFile);
                if (schematic == null) {
                    Main.getInstance().getLogger().warning("Schematic non-existent or not in gzip format");
                    return;
                }
                SchematicHandler.manager.paste(schematic, plot, 0, 0, new RunnableVal<Boolean>() {
                    @Override
                    public void run() {
                        if (!value) {
                            Main.getInstance().getLogger().warning("Schematic paste error");
                        }
                    }
                });
            }
        });

        return plot;

    }

    public void addFreePlot(Plot plot) {
        TaskManager.runTaskAsync(new Runnable() {
            @Override
            public void run() {
                final long start = System.currentTimeMillis();
                final boolean result = MainUtil.clear(plot, true, new Runnable() {
                    @Override
                    public void run() {
                        SetBlockQueue.addNotify(new Runnable() {
                            @Override
                            public void run() {
                                plot.removeRunning();
                                // If the state changes, then mark it as no longer done
                                if (FlagManager.getPlotFlagRaw(plot, "done") != null) {
                                    FlagManager.removePlotFlag(plot, "done");
                                }
                                if (FlagManager.getPlotFlagRaw(plot, "analysis") != null) {
                                    FlagManager.removePlotFlag(plot, "analysis");
                                }
                                Main.getInstance().getLogger().info("Cleared" + (System.currentTimeMillis() - start));
                                RoomManager.this.freePlots.push(plot);
                            }
                        });
                    }
                });
                if (result) {
                    plot.addRunning();
                }
            }
        });
    }

    public void unregisterRoom(Room room) {
        //TODO: убрать руму из списка
    }

    public Plot getFreePlot() {
        return freePlots.poll();
    }
}
