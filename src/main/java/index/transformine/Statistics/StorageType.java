/*
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                     Version 2, December 2004
 *
 *  Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 *  Everyone is permitted to copy and distribute verbatim or modified
 *  copies of this license document, and changing it is allowed as long
 *  as the name is changed.
 *
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *   0. You just DO WHAT THE FUCK YOU WANT TO.
 *
 */

package index.transformine.Statistics;

public enum StorageType {
	DATABASE("database"), YAML("yaml");

	private String name = null;

	StorageType(String configName) {
		this.name = configName;
	}

	public String getName() {
		return this.name;
	}

	public static StorageType getByName(String name) {
		for (StorageType type : StorageType.values()) {
			if (type.getName().equals(name)) {
				return type;
			}
		}

		return StorageType.YAML;
	}
}
