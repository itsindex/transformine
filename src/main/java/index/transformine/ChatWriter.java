/*
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                     Version 2, December 2004
 *
 *  Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 *  Everyone is permitted to copy and distribute verbatim or modified
 *  copies of this license document, and changing it is allowed as long
 *  as the name is changed.
 *
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *   0. You just DO WHAT THE FUCK YOU WANT TO.
 *
 */

package index.transformine;

import org.bukkit.ChatColor;

/**
 * Created by ksnzk on 24.02.2016.
 */
public class ChatWriter {
    public ChatWriter() {
        super();
    }

    public static String pluginMessage(String str) {
        return ChatColor.translateAlternateColorCodes('&', Main.getInstance().getConfig().getString("chat-prefix", ChatColor.GRAY + "[" + ChatColor.AQUA + "Transformine" + ChatColor.GRAY + "]"))
                + ChatColor.WHITE + str;
    }

}
