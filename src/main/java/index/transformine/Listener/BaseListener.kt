/*
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                     Version 2, December 2004
 *
 *  Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 *  Everyone is permitted to copy and distribute verbatim or modified
 *  copies of this license document, and changing it is allowed as long
 *  as the name is changed.
 *
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *   0. You just DO WHAT THE FUCK YOU WANT TO.
 *
 */

package index.transformine.Listener

import index.transformine.Main
import org.bukkit.event.Listener

/**
 * Created by ksnzk on 03.03.2016.
 */
abstract class BaseListener : Listener {

    init {
        this.registerEvents()
    }

    private fun registerEvents() {
        Main.getInstance().server.pluginManager.registerEvents(this, Main.getInstance())
    }

}